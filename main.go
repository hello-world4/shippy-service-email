package main

import (
	"github.com/micro/go-micro/v2"
	pb "gitlab.com/hello-world4/shippy-service-user/proto/user"
	"log"
	"context"
)

const eventTopic = "user.created"

type Subscriber struct{}

func(sub *Subscriber) Process(ctx context.Context, user *pb.User) error {
	log.Println("Picked up a new message")
	log.Println("Sending email to:", user.Email)
	return nil
}

func main() {
	srv := micro.NewService(
		micro.Name("shippy.service.email"),
	)

	srv.Init()

	if err := micro.RegisterSubscriber(eventTopic, srv.Server(), new(Subscriber)); err != nil {
		log.Println("RegisterSubscriber error: ", err)
	}

	if err := srv.Run(); err != nil {
		log.Println(err)
	}
}

//TODO: send email via nats
func sendEmail(user *pb.User) error {
	log.Println("Sending email to :", user.Email)
	return nil
}