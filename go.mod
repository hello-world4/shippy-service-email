module gitlab.com/hello-world4/shippy-service-email

go 1.15

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

require (
	github.com/micro/go-micro/v2 v2.9.1
	gitlab.com/hello-world4/shippy-service-user v0.0.0-20210216105839-60ba64f0b339
)
